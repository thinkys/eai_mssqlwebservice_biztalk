namespace UseWS_BT {
    using Microsoft.XLANGs.BaseTypes;
    
    
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.BizTalk.Schema.Compiler", "3.0.1.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [SchemaType(SchemaTypeEnum.Document)]
    [Schema(@"http://UseWS_BT.AddAmount",@"AddAmount")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::UseWS_BT.PropertySchema.uid), XPath = @"/*[local-name()='AddAmount' and namespace-uri()='http://UseWS_BT.AddAmount']/*[local-name()='uid' and namespace-uri()='']", XsdType = @"int")]
    [Microsoft.XLANGs.BaseTypes.PropertyAttribute(typeof(global::UseWS_BT.PropertySchema.amount), XPath = @"/*[local-name()='AddAmount' and namespace-uri()='http://UseWS_BT.AddAmount']/*[local-name()='amount' and namespace-uri()='']", XsdType = @"int")]
    [System.SerializableAttribute()]
    [SchemaRoots(new string[] {@"AddAmount"})]
    [Microsoft.XLANGs.BaseTypes.SchemaReference(@"UseWS_BT.PropertySchema.PropertySchema", typeof(global::UseWS_BT.PropertySchema.PropertySchema))]
    public sealed class AddAmount : Microsoft.XLANGs.BaseTypes.SchemaBase {
        
        [System.NonSerializedAttribute()]
        private static object _rawSchema;
        
        [System.NonSerializedAttribute()]
        private const string _strSchema = @"<?xml version=""1.0"" encoding=""utf-16""?>
<xs:schema xmlns=""http://UseWS_BT.AddAmount"" xmlns:b=""http://schemas.microsoft.com/BizTalk/2003"" xmlns:ns0=""https://UseWS_BT.PropertySchema"" targetNamespace=""http://UseWS_BT.AddAmount"" xmlns:xs=""http://www.w3.org/2001/XMLSchema"">
  <xs:annotation>
    <xs:appinfo>
      <b:imports>
        <b:namespace prefix=""ns0"" uri=""https://UseWS_BT.PropertySchema"" location=""UseWS_BT.PropertySchema.PropertySchema"" />
      </b:imports>
    </xs:appinfo>
  </xs:annotation>
  <xs:element name=""AddAmount"">
    <xs:annotation>
      <xs:appinfo>
        <b:properties>
          <b:property name=""ns0:uid"" xpath=""/*[local-name()='AddAmount' and namespace-uri()='http://UseWS_BT.AddAmount']/*[local-name()='uid' and namespace-uri()='']"" />
          <b:property name=""ns0:amount"" xpath=""/*[local-name()='AddAmount' and namespace-uri()='http://UseWS_BT.AddAmount']/*[local-name()='amount' and namespace-uri()='']"" />
        </b:properties>
      </xs:appinfo>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name=""uid"" type=""xs:int"" />
        <xs:element name=""amount"" type=""xs:int"" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>";
        
        public AddAmount() {
        }
        
        public override string XmlContent {
            get {
                return _strSchema;
            }
        }
        
        public override string[] RootNodes {
            get {
                string[] _RootElements = new string [1];
                _RootElements[0] = "AddAmount";
                return _RootElements;
            }
        }
        
        protected override object RawSchema {
            get {
                return _rawSchema;
            }
            set {
                _rawSchema = value;
            }
        }
    }
}
